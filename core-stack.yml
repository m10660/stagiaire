version: '3.2'

secrets:
  portainer-pass:
    external: true
services:

  portainer-agent:
    image: portainer/agent:2.13.1
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - /var/lib/docker/volumes:/var/lib/docker/volumes
    networks:
      - agent_network
    deploy:
      mode: global
      placement:
        constraints: [node.platform.os == linux]

  portainer:
    image: portainer/portainer-ce:2.13.1
    command: -H tcp://tasks.portainer-agent:9001 --tlsskipverify --admin-password-file '/run/secrets/portainer-pass'
    secrets: 
      - portainer-pass
    volumes:
      - portainer_data:/data
    networks:
      - agent_network
      - traefik-net
    deploy:
      mode: replicated
      replicas: 1
      placement:
        constraints: [node.role == manager]
      labels:
        - "traefik.enable=true"
        - "traefik.http.routers.portainer.entrypoints=http"
        - "traefik.http.routers.portainer.rule=Host(`portainer.localhost`)"
        - "traefik.http.services.portainer.loadbalancer.server.port=9000"

  traefik:
    image: traefik:v2.7
    command: 
      - "--log.level=DEBUG"
      - "--api.insecure=true"
      - "--providers.docker"
      - "--providers.docker.swarmMode=true"
      - "--providers.docker.network=traefik-net"
      - "--providers.docker.exposedbydefault=false"
      - "--entrypoints.http.address=:80"
      - "--entrypoints.ssh.address=:22"
    ports:
      - "80:80"
      - "22:22"
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
    networks:
      - traefik-net
    deploy:
      labels:
        - traefik.enable=true
        - traefik.http.routers.traefik.entrypoints=http
        - traefik.http.routers.traefik.rule=Host(`traefik.localhost`)
        - traefik.http.services.traefik.loadbalancer.server.port=8080

  whoami:
    image: traefik/whoami
    networks:
      - traefik-net
    deploy:
      labels:
        - traefik.enable=true
        - traefik.http.routers.whoami.entrypoints=http
        - traefik.http.routers.whoami.rule=Host(`whoami.localhost`)
        - traefik.http.services.whoami-service.loadbalancer.server.port=80

  mydev:
    image: mydev:latest    
    volumes:
      - mydev_data:/root
    networks:
      - traefik-net
      - db-net
    deploy:
      labels:
        - traefik.enable=true
        - traefik.http.routers.mydev.entrypoints=http
        - traefik.http.routers.mydev.rule=Host(`mydev.localhost`)
        - traefik.http.routers.mydev.service=mydev-service
        - traefik.http.services.mydev-service.loadbalancer.server.port=8080
        - traefik.http.routers.mydev-api.entrypoints=http
        - traefik.http.routers.mydev-api.rule=Host(`api.mydev.localhost`)
        - traefik.http.routers.mydev-api.service=mydev-api-service
        - traefik.http.services.mydev-api-service.loadbalancer.server.port=3000
        - "traefik.tcp.routers.mydev-ssh.rule=HostSNI(`*`)"
        - "traefik.tcp.routers.mydev-ssh.entrypoints=ssh"
        - "traefik.http.routers.mydev-ssh.tls=true"
        - "traefik.tcp.routers.mydev-ssh.service=mydev-ssh-service"
        - "traefik.tcp.services.mydev-ssh-service.loadbalancer.server.port=22"

  mydb:
    image: postgres:14.3-alpine3.16
    ports:
      - 5432:5432
    networks:
      - db-net
    environment:
      POSTGRES_PASSWORD: postgres

  dbeaver:
    image: dbeaver/cloudbeaver:22.0.5
    volumes:
      - dbeaver_data:/opt/cloudbeaver/workspace
    networks:
      - traefik-net
      - db-net
    deploy:
      labels:
        - traefik.enable=true
        - traefik.http.routers.dbeaver.entrypoints=http
        - traefik.http.routers.dbeaver.rule=Host(`dbeaver.localhost`)
        - traefik.http.services.dbeaver-service.loadbalancer.server.port=8978

networks:
  agent_network:
    driver: overlay
    attachable: true
  traefik-net:
    external: true
  db-net:

volumes:
  portainer_data:
  mydev_data:
  dbeaver_data:
