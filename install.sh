#!/bin/bash

# ----------------------------------
# Colors
# ----------------------------------
NOCOLOR='\033[0m'
RED='\033[0;31m'
GREEN='\033[0;32m'
ORANGE='\033[0;33m'
BLUE='\033[0;34m'
PURPLE='\033[0;35m'
CYAN='\033[0;36m'
LIGHTGRAY='\033[0;37m'
DARKGRAY='\033[1;30m'
LIGHTRED='\033[1;31m'
LIGHTGREEN='\033[1;32m'
YELLOW='\033[1;33m'
LIGHTBLUE='\033[1;34m'
LIGHTPURPLE='\033[1;35m'
LIGHTCYAN='\033[1;36m'
WHITE='\033[1;37m'


if [ -z "$SUDO_USER" ] ; then
    echo "You must run this script with sudo!"
    exit 0
fi

home=$(awk -v u="$SUDO_USER" -v FS=':' '$1==u {print $6}' /etc/passwd)

options=("Install Visual Studio Code" "Install DBeaver" "Install Docker" "Deploy core stack" "Configure SSH" "Clone and start webapp template")
 
# custom menu

custom_menu() {
    echo "Available options:"
    for i in ${!options[@]}; do
        printf "%3d%s) %s\n" $((i+1)) "${choices[i]:- }" "${options[i]}"
    done
    [[ "$msg" ]] && echo "$msg"; :
}

ok() {
    echo -e " ${GREEN}OK${NOCOLOR}" >&3
}

fail() {
    echo -e " ${RED}FAIL${NOCOLOR}" >&3
}
check() {
    if [ $? -eq 0 ]; then
        ok
    else
        fail
    fi
}

prompt="Check an option (again to uncheck, ENTER when done): "
while custom_menu && read -rp "$prompt" num && [[ "$num" ]]; do
    [[ "$num" != *[![:digit:]]* ]] &&
    (( num > 0 && num <= ${#options[@]} )) ||
    { msg="Invalid option: $num"; continue; }
    ((num--)); msg="${options[num]} was ${choices[num]:+un}checked"
    [[ "${choices[num]}" ]] && choices[num]="" || choices[num]="+"
done
 
printf "You selected"; msg=" nothing"
for i in ${!options[@]}; do
    [[ "${choices[i]}" ]] && { printf " %s" "${options[i]}"; msg=""; }
done
echo "$msg"

exec 3>&1 4>&2
exec 1>log.out 2>&1
set -x

if [[ ${!choices[*]} =~ 0 ]]; then
    echo -e "${YELLOW}### Visual Studio Code ###${NOCOLOR}" >&3
    echo -n "Updating the packages index" >&3
    apt-get update -y 
    check
    echo -n "Installing the dependencies" >&3
    apt-get install -y gnupg2 software-properties-common apt-transport-https curl
    check
    echo -n "Importing the Microsoft GPG key" >&3
    curl -sSL https://packages.microsoft.com/keys/microsoft.asc | apt-key add -
    check
    echo -n "Adding the Visual Studio Code repository to your system" >&3
    add-apt-repository "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main"
    check
    echo -n "Updating the packages index" >&3
    apt-get update -y 
    check
    echo -n "Installing the Visual Studio Code package" >&3
    apt-get install -y code 
    check
    echo -n "Installing Remote SSH extension within VS Code" >&3
    runuser $SUDO_USER -c 'code  --install-extension  ms-vscode-remote.remote-ssh'
    check
    # runuser $SUDO_USER -c 'code  --install-extension  ms-azuretools.vscode-docker' 
fi

if [[ ${!choices[*]} =~ 1 ]]; then
    echo -e "${YELLOW}### DBeaver ###${NOCOLOR}" >&3
    echo -n "Updating the packages index" >&3
    apt-get update -y 
    check
    echo -n "Installing the dependencies" >&3
    apt-get install -y gnupg2 software-properties-common apt-transport-https curl
    check
    echo -n "Importing the DBeaver GPG key" >&3
    curl -sSL https://dbeaver.io/debs/dbeaver.gpg.key | apt-key add -
    check
    echo -n "Adding the DBeaver repository to your system" >&3
    add-apt-repository "deb https://dbeaver.io/debs/dbeaver-ce /"
    check   
    echo -n "Updating the packages index" >&3
    apt-get update -y 
    check
    echo -n "Installing the DBeaver package" >&3
    apt-get install -y dbeaver-ce 
    check
    echo -n "Configuring localhost DB connection" >&3
    runuser $SUDO_USER -c "cp -Rf DBeaverData $home/.local/share"
    check
    echo -n "Downloading PostgreSQL JDBC Driver" >&3
    dbeaver_dir=$home/.local/share/DBeaverData/workspace6
    runuser $SUDO_USER -c "curl -sSL https://jdbc.postgresql.org/download/postgresql-42.3.6.jar -o $dbeaver_dir/postgresql-42.3.6.jar"
    check
    # runuser $SUDO_USER -c "dbeaver -help -nosplash -q &> /dev/null"    
 
fi

if [[ ${!choices[*]} =~ 2 ]]; then
    # Remove old docker installation
    apt-get purge -y docker docker-compose docker-doc docker-registry docker.io docker2aci python3-docker python3-dockerpty 
    apt-get autoremove -y --purge docker docker-compose docker-doc docker-registry docker.io docker2aci python3-docker python3-dockerpty
    rm -rf /var/lib/docker /etc/docker
    rm -rf /var/run/docker.sock

    # Install docker and enable swarm
    apt-get install -y docker*
    docker --version
    usermod -aG docker $USER
    chmod 666 /var/run/docker.sock
    ip=$(hostname -I | awk '{print $1}')
    docker swarm init --advertise-addr $ip
    docker system info
fi

if [[ ${!choices[*]} =~ 3 ]]; then
    # Deploy core stack
    docker network create -d overlay traefik-net
    printf "superpassword" | docker secret create portainer-pass -
    docker build -t mydev:latest .
    apt-get install -y httping
    docker stack deploy -c core-stack.yml core
    for value in  http://whoami.localhost http://portainer.localhost http://traefik.localhost/dashboard http://dbeaver.localhost
    do
        echo -n "Waiting for $value."
        until httping  -q -c1 -s -o 200 -g "$value"; do   echo -n "."; done
        echo "OK"
    done
fi

if [[ ${!choices[*]} =~ 4 ]]; then
    # Removes all ssh keys belonging to localhost

    ssh_dir=$home/.ssh
    known_hosts=$ssh_dir/known_hosts
    if test -f "$known_hosts"; then
        ssh-keygen -f "$known_hosts" -R "localhost"
        chown $SUDO_USER $known_hosts
    fi

    # Configure ssh client
    mkdir -p $ssh_dir
    chmod 700 $ssh_dir
    cp ./ssh/config $ssh_dir
    cp ./ssh/ssh-key $ssh_dir
    chown -R $SUDO_USER:$SUDO_USER $ssh_dir
    chmod 600 $ssh_dir/*
    echo -n "Waiting for ssh connection."
    until su - $SUDO_USER -c "ssh dev exit"; do   echo -n "."; done
    echo "OK"
fi

if [[ ${!choices[*]} =~ 5 ]]; then
    # Deploy webapp-template
    # Get container id
    container=$(docker ps -f name=core_mydev --format "{{.ID}}")
    # Clone repo
    docker exec -it -u root -w /root $container  rm -Rf /root/webapp-template
    docker exec -it -u root -w /root $container  git clone https://gitlab.com/m10660/webapp-template.git
    # Start app
    docker exec -it -u root $container bash -c 'echo "export DATABASE_URL=postgres://postgres:postgres@mydb:5432/postgres" > /root/webapp-template/config.ini'
    sudo docker exec -d -u root -w /root/webapp-template $container ./start.sh
    echo -n "Waiting for http://api.mydev.localhost."
    until httping -q -c1 -s -o 200 -g "http://api.mydev.localhost"; do echo -n "."; done
    echo "OK"
fi





