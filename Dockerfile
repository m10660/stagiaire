
FROM debian:11.3-slim

ENV DEBIAN_FRONTEND=noninteractive
ENV DEBCONF_NOWARNINGS="yes"

## Refresh repos
RUN apt-get update

## Install and configure ssh to root
RUN apt-get install -y openssh-server 
COPY ssh/sshd_config /etc/ssh/sshd_config
COPY ssh/authorized_keys /etc/ssh/keys/root/authorized_keys
RUN service ssh start
EXPOSE 22

## Install node16
RUN apt-get install -y curl 
RUN curl -fsSL https://deb.nodesource.com/setup_16.x | bash -
RUN apt-get update && apt-get install -y nodejs

# Install Yarn
RUN curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | gpg --dearmor | tee /usr/share/keyrings/yarnkey.gpg >/dev/null
RUN echo "deb [signed-by=/usr/share/keyrings/yarnkey.gpg] https://dl.yarnpkg.com/debian stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get update && apt-get install yarn

## Install git
RUN apt-get install -y git

## Install quasar
RUN yarn global add @quasar/cli@1.3.2
EXPOSE 8080

CMD ["/usr/sbin/sshd","-D"]

